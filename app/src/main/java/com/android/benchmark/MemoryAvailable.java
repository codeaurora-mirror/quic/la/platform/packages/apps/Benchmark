/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.benchmark;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class MemoryAvailable extends Activity {
    private int mTargetPlainMemory;
    private int mTargetBitmaps;
    private int mTargetTextures;
    private int mTargetCompute;
    private int mTestLoadNumber = 0;
    private boolean mDoReset = true;

    private TextView mTextResult1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memory_available);
        android.util.Log.e("bench", "ma onCreate");
        mDoReset = true;

        mTextResult1 = (TextView) findViewById(R.id.memory_available_test_result1);
    }

    protected void onResume() {
        super.onResume();
        android.util.Log.e("bench", "ma onResume");

        Intent i = getIntent();
        android.util.Log.e("bench", "intent " + i);
        if (mDoReset) {
            mTargetPlainMemory = 10;
            mTargetBitmaps = 0;
            mTargetTextures = 0;
            mTargetCompute = 0;
        }
        android.util.Log.e("bench", "doReset " + mDoReset);


        if (mDoReset) {
            startSubActivity(MemoryAvailableLoad1.class, 5, 1);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_memory_available, menu);
        return true;
    }

    private void startSubActivity(final Class c, final int delay, final int doInit) {
        final MemoryAvailable ma = this;
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(ma, c);
                intent.putExtra("reset", doInit);
                intent.putExtra("plain", mTargetPlainMemory);
                intent.putExtra("bitmaps", mTargetBitmaps);
                intent.putExtra("textures", mTargetTextures);
                intent.putExtra("compute", mTargetCompute);
                startActivityForResult(intent, 0);
            }
        }, (1000 * delay));
    }

    private void postTextToView(TextView v, String s) {
        final TextView tv = v;
        final String ts = s;

        v.post(new Runnable() {
            public void run() {
                tv.setText(ts);
            }
        });

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        android.util.Log.e("bench", "ma onActivityResult");

        if (requestCode == 0) {
            android.util.Log.e("bench", "ma result " + resultCode);

            if (resultCode == RESULT_OK) {
                mDoReset = false;

                mTargetPlainMemory = data.getIntExtra("plain", 0);
                mTargetBitmaps = data.getIntExtra("bitmaps", 0);
                mTargetTextures = data.getIntExtra("textures", 0);
                mTargetCompute = data.getIntExtra("compute", 0);

                postTextToView(mTextResult1, "Passed at " + mTargetPlainMemory + " MB");
                mTargetPlainMemory += 100;
                mTestLoadNumber ++;

                if ((mTestLoadNumber & 1) != 0) {
                    startSubActivity(MemoryAvailableLoad2.class, 5, 0);
                } else {
                    startSubActivity(MemoryAvailableLoad1.class, 5, 0);
                }
            } else {
                postTextToView(mTextResult1, "Failed at " + mTargetPlainMemory + " MB");
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
